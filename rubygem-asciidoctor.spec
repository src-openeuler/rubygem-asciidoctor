%global gem_name asciidoctor
Name:           rubygem-asciidoctor
Version:        2.0.23
Release:        1
Summary:        An implementation of the AsciiDoc text processor and publishing
License:        MIT
URL:            https://github.com/asciidoctor/asciidoctor
Source0:        https://github.com/asciidoctor/asciidoctor/archive/v%{version}/%{gem_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  ruby(release) rubygems-devel ruby(rubygems)
Requires:       ruby(release)
Provides:       asciidoctor = %{version} rubygem(asciidoctor) = %{version}

%description
Asciidoctor reads and parses text written in the AsciiDoc syntax, then feeds the parse tree to a
set of built-in converters to produce HTML5, DocBook 5 (or 4.5) or man(ual) page output. You have
the option of using your own converter or loading Tilt-supported templates to customize the generated
output or produce additional formats.

%package help
Summary:   Rubygem-asciidoctor documentation
Requires:  %{name} = %{version}-%{release}
BuildArch: noarch
Provides:  %{name}-doc = %{version}-%{release}
Obsoletes: %{name}-doc < %{version}-%{release}

%description help
This package provides help documents for rubygem-asciidoctor.

%prep
%autosetup -n %{gem_name}-%{version} -p1

sed -i -e 's/#\(s\.test_files\)/\1/' %{gem_name}.gemspec
sed -i -e 's|#!/usr/bin/env ruby|#!/usr/bin/ruby|' bin/%{gem_name}

%build
gem build %{gem_name}.gemspec
%gem_install -n %{gem_name}-%{version}.gem

%install
install -d %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* %{buildroot}%{gem_dir}/

install -d %{buildroot}%{_bindir}
cp -a .%{_bindir}/* %{buildroot}%{_bindir}/

install -d %{buildroot}%{_mandir}/man1
cp -a .%{gem_instdir}/man/*.1 %{buildroot}%{_mandir}/man1/

rm -fr %{buildroot}%{gem_cache}
rm -f  %{buildroot}%{gem_instdir}/asciidoctor.gemspec
rm -fr %{buildroot}%{gem_instdir}/man
rm -fr %{buildroot}%{gem_instdir}/test
rm -fr %{buildroot}%{gem_instdir}/features
rm -f  %{buildroot}%{gem_instdir}/LICENSE

%files
%dir %{gem_instdir}
%doc %{gem_instdir}/CHANGELOG.adoc
%doc %{gem_instdir}/README.*
%lang(de) %doc %{gem_instdir}/README-de.*
%lang(fr) %doc %{gem_instdir}/README-fr.*
%lang(jp) %doc %{gem_instdir}/README-jp.*
%lang(zh_CN) %doc %{gem_instdir}/README-zh_CN.*
%{gem_instdir}/data
%{_bindir}/*
%{gem_instdir}/bin
%{gem_libdir}
%{gem_spec}

%files help
%doc %{gem_docdir}
%{_mandir}/man1/*


%changelog
* Thu Jul 25 2024 Funda Wang <fundawang@yeah.net> - 2.0.23-1
- Update to 2.0.23
- drop abused exclude, delete them in install section instead

* Tue Feb 02 2023 wenchaofan <349464272@qq.com> - 2.0.18-1
- Update to 2.0.18

* Tue Dec 01 2020 lingsheng <lingsheng@huawei.com> - 2.0.12-1
- Update to 2.0.12

* Wed Jan 8 2020 duyeyu <duyeyu@huawei.com> - 1.5.6.1-5
- Package init
